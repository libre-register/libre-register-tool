/*
 * Converts an option to a string
 */
pub fn op_to_string(string: Option<String>) -> String {
    match string {
        None => "".to_string(),
        Some(string) => string,
    }
}
