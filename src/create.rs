pub fn create(mut args: std::env::Args) {
    // Skips past the argument on the programs name and the operation
    args.next();
    args.next();

    // Broadcasts to the user that we have started making a database
    uquery::notice("Database Creation", 1);

    // Checks if a file path is specified
    let file_path = args.next();

    match file_path {
        Some(file_path) => {
            // Gets the name of the school from the user.
            let name = uquery::string("What is the name of your school");

            // Tells the user that we are beggining writing
            uquery::notice(format!("Saving file to \"{}\"", file_path,).as_str(), 2);

            // Creates a LrDB.Database
            lrdb::database::Database::new(file_path.as_str(), name);

            uquery::notice(format!("Writing to file completed.",).as_str(), 2)
        }
        None => {
            eprintln!("Error! No file specified. Please rerun with a file specified");
        }
    }
}
