pub fn name(prefixes: &str, first: &str, middle: &str, last: &str) -> String {
    let mut string = "".to_string();

    string.push(' ');
    string.push_str(prefixes);
    string = string.trim().to_string();

    string.push(' ');
    string.push_str(first);
    string = string.trim().to_string();

    string.push(' ');
    string.push_str(middle);
    string = string.trim().to_string();

    string.push(' ');
    string.push_str(last);
    string = string.trim().to_string();

    if string.is_empty() {
        "NoName".to_string()
    } else {
        string
    }
}

pub fn name_op(
    prefixes: &Option<&str>,
    first: &Option<&str>,
    middle: &Option<&str>,
    last: &Option<&str>,
) -> String {
    name(
        {
            match prefixes {
                Some(prefixes) => prefixes,
                _ => "",
            }
        },
        {
            match first {
                Some(first) => first,
                _ => "",
            }
        },
        {
            match middle {
                Some(middle) => middle,
                _ => "",
            }
        },
        {
            match last {
                Some(last) => last,
                _ => "",
            }
        },
    )
}

pub fn contact_name(contact: &vcard::VCard) -> String {
    if contact.names != None {
        // Checks the contact has a name
        for name in contact.names.as_ref().unwrap().iter() {
            return crate::formatting::name(
                op_comp_to_string(name.value.get_name_prefix()).as_str(),
                op_comp_to_string(name.value.get_first_name()).as_str(),
                op_comp_to_string(name.value.get_middle_name()).as_str(),
                op_comp_to_string(name.value.get_last_name()).as_str(),
            );
        }
    }
    "".to_string()
}

fn op_comp_to_string(comp: Option<&vcard::values::text::Component>) -> String {
    match comp {
        Some(comp) => comp.to_string(),
        _ => "".to_string(),
    }
}
