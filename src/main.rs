mod create;
mod modify;

fn main() -> Result<(), Box<dyn std::error::Error>>{
    // Starts the Logger
    env_logger::init();

    let args: Vec<String> = std::env::args().collect();
    if args.len() >= 2 {
        let option = &args[1];
        match option.as_str() {
            "create" => create::create(std::env::args()),
            "modify" => modify::modify(std::env::args())?,
            _ => help(),
        }
        Ok(())
    } else {
        help();
        Ok(())
    }
}

fn help() {
    println!(include_str!("../data/help.txt"));
}
