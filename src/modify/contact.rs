use crate::modify::contact_information::update_contact_contact_information;
use contack::Contact;
use log::*;
use std::fmt::Write;

pub fn modify_contacts(file: String) {
    let mut db = lrdb::database::Database::new_from_existing(file.as_str());
    // Prints the choice for the user and gets their query.
    println!(include_str!("../../data/modify/contact/contacts.txt"));
    let choice = uquery::num::<u8>("Enter Choice");

    match choice {
        1 => add_contact(&mut db),
        2 => modify_contact(&mut db),
        3 => delete_contact(&mut db),
        _ => {
            uquery::notice("Exiting Now", 3);
            return ();
        }
    }
}

// Adds a contact
fn add_contact(db: &mut lrdb::database::Database) {
    // Tells the user of our progress
    uquery::notice("Creating Contact", 3);

    // Creates a Contact
    let mut contact = Contact::new(contack::name::Name::new(None, None, None, None, None));

    // Commit it to the database.
    update_contact_name(&mut contact);
    db.add_contact(&mut contact);

    // Tells the user of our progress
    uquery::notice("Created Contact", 3);
}

fn modify_contact(db: &mut lrdb::database::Database) {
    let contacts = db.restore_all_contacts();

    match contacts {
        Ok(mut contacts) => {
            if contacts.len() >= 1 {
                let selected_num = select_contact(&contacts);
                // Queries the user for a contact
                let contact = &mut contacts[selected_num];

                loop {
                    // Prints the user's available input
                    println!(include_str!("../../data/modify/contact/contact.txt"));

                    // Asks the user for input
                    let choice = uquery::num::<u8>("Enter Choice");

                    // Checks waht the user has answered
                    match choice {
                        0 => {
                            db.update_contact(contact);
                            return (); // Quit
                        }
                        1 => {
                            update_contact_name(contact);
                        }
                        2 => {
                            update_contact_birthdate(contact);
                        }
                        3 => {
                            update_contact_address(contact);
                        }
                        4 => {
                            update_contact_contact_information(contact);
                        }
                        5 => {
                            update_contact_org(contact);
                        }
                        6 => {
                            update_contact_roles(contact);
                        }
                        _ => println!("Invalid Choice!"), // Invalid choice
                    }
                }
            }
        }
        Err(e) => {
            uquery::error(
                format!("Error Reading Contacts: {}", &e.to_string()).as_str(),
                None,
            );
        }
    }
}

fn delete_contact(db: &mut lrdb::database::Database) {
    let contacts = db.restore_all_contacts();

    // Error Handling
    match contacts {
        Ok(mut contacts) => {
            if contacts.len() >= 1 {
                // Queries the user for a contact
                let contact = select_contact(&contacts);

                // Deletes the contact
                db.delete_contact(contacts.get_mut(contact).unwrap());
            }
        }
        Err(e) => uquery::error(
            &format!("Error selecting contact, please try again. \n {}", e),
            None,
        ),
    }
}

pub fn select_contact(contacts: &Vec<Contact>) -> usize {
    // User Instructions
    println!("Please select a contact.");
    uquery::query(
        "Press enter to list the contacts, \
		remember the number next to the contact."
            .to_string(),
    );
    println!();

    // Lists all the contacts
    list_contacts(contacts);
    println!();

    // Figures out what contact the user has selected
    let contact = std::cmp::min(
        std::cmp::max(
            uquery::num::<usize>(
            	"Enter the contact number"
        	),
        0),
        contacts.len() - 1,
    );

    uquery::notice(
        format!("Selected {}", contacts[contact].name.to_string()).as_str(),
        3,
    );

    contact
}

fn list_contacts(contacts: &Vec<Contact>) {
    // Creates a pager to pipe the output to.
    let mut pager = minus::Pager::new();

    let mut n: u64 = 0;
    // Loops through the contacts
    for contact in contacts {
        match writeln!(pager.lines, " {:>6} | {}", n, contact.name.to_string()) {
            Ok(()) => {}
            _ => {
                eprintln!("Error Writing Line.")
            }
        }
        n += 1;
    }

    match minus::page_all(pager) {
        Ok(()) => {}
        _ => {
            eprintln!("Error Opening Pager.")
        }
    }
}

fn update_contact_name(contact: &mut Contact) {
    // Query the user for the input
    let first_name = uquery::string_with_default_reference(
        "What is the first name",
        contact.name.given.as_ref(),
    );
    let last_name = uquery::string_with_default_reference(
        "What is the last name",
        contact.name.family.as_ref(),
    );
    let middle_name = uquery::string_with_default_reference(
        "Enter any middle names",
        contact.name.additional.as_ref(),
    );
    let prefixes_name =
        uquery::string_with_default_reference("Enter any prefixes", contact.name.prefixes.as_ref());

    // Sets the values
    contact.name.given = Some(first_name);
    contact.name.family = Some(last_name);
    contact.name.additional = Some(middle_name);
    contact.name.prefixes = Some(prefixes_name);
}
pub fn update_contact_birthdate(contact: &mut Contact) {
    // Queries the user
    let year = uquery::num::<i64>("Please Enter the Year");
    let month = uquery::num::<u8>("Please Enter the Month");
    let day = uquery::num::<u8>("Please Enter the Day");

    // Sets it to the contact
    contact.bday = Some(contack::date_time::DateTime::new(
        year, month, day, 0, 0,
        0, // Why did i even include seconds and minutes and hours?? Who even knows there birthday to that precision
    ))
}

pub fn update_contact_address(contact: &mut Contact) {
    // Prints the user's available input
    println!(include_str!("../../data/modify/contact/addresses.txt"));

    // Asks the user for input
    /*
     * 1 = Work; 2 = Home
     */
    let typ_choice = uquery::num::<u8>("Enter Choice");
    if typ_choice >= 2 || typ_choice == 0 {
        println!("Invalid Choice, Exiting now.");
        return ();
    }

    // Finds the previous address
    let prev_adr = match typ_choice {
        1 => contact.work_address.as_ref(),
        2 => contact.home_address.as_ref(),
        _ => {
            println!("Unknown Choice, Exiting Now!");
            return ();
        }
    };

    // Creates the address
    let address = contack::address::Address::new(
        // Street
        Some(uquery::string_with_default_reference("Enter the street", {
            match prev_adr {
                None => None,
                Some(ref prev_adr) => prev_adr.street.as_ref(),
            }
        })),
        // Locality
        Some(uquery::string_with_default_reference(
            "Enter the city / village / town",
            {
                match prev_adr {
                    None => None,
                    Some(ref prev_adr) => prev_adr.locality.as_ref(),
                }
            },
        )),
        // Region
        Some(uquery::string_with_default_reference("Enter the region", {
            match prev_adr {
                None => None,
                Some(ref prev_adr) => prev_adr.region.as_ref(),
            }
        })),
        // Code
        Some(uquery::string_with_default_reference(
            "Enter the postal code",
            {
                match prev_adr {
                    None => None,
                    Some(ref prev_adr) => prev_adr.code.as_ref(),
                }
            },
        )),
        // Country
        Some(uquery::string_with_default_reference(
            "Enter the country",
            {
                match prev_adr {
                    None => None,
                    Some(ref prev_adr) => prev_adr.country.as_ref(),
                }
            },
        )),
    );

    // Sets the new address
    match typ_choice {
        1 => contact.work_address = Some(address),
        2 => contact.home_address = Some(address),
        _ => {
            error!("Error with addresses.")
        } // Imposible
    };
}

fn update_contact_roles(contact: &mut Contact) {
    // Sets the job title
    let title = uquery::string_with_default_reference(
        "Enter the contact's job title",
        contact.title.as_ref(),
    );
    // Does the job role
    contact.title = Some(title);
    let role = uquery::string_with_default_reference(
        "Enter the contact's job role",
        contact.role.as_ref(),
    );
    contact.role = Some(role);
}

fn update_contact_org(contact: &mut Contact) {
    contact.org = Some(contack::org::Org::new(
        uquery::string_with_default_reference("Enter the organisation", {
            match &contact.org {
                Some(org) => Some(&org.org),
                None => None,
            }
        }),
        uquery::string_with_default_reference("Enter the unit", {
            match &contact.org {
                Some(org) => Some(&org.unit),
                None => None,
            }
        }),
        uquery::string_with_default_reference("Enter the office / sub unit", {
            match &contact.org {
                Some(org) => Some(&org.office),
                None => None,
            }
        }),
    ))
}
