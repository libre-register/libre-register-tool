use contack::contact_information::ContactInformation;
use contack::contact_information::Type;
use contack::contact_platform::ContactPlatform;
use contack::Contact;
use log::*;
use std::fmt::Write;

pub fn update_contact_contact_information(contact: &mut Contact) {
    loop {
        // Prints the user's available choice
        println!(include_str!("../../data/modify/contact_information/mod.txt"));

        //  Gets the users choice
        match uquery::num::<u8>("Enter Choice") {
            1 => add_contact_information(contact), // Add
            2 => {
                // Modify
                match select_contact(contact) {
                    Ok(n) => modify_contact_information(&mut contact.contact_information[n]),
                    Err(e) => warn!("{}", e),
                }
            }
            3 => {
                // Delete
                match select_contact(contact) {
                    Ok(n) => {
                        contact.contact_information.remove(n);
                    }
                    Err(e) => warn!("{}", e),
                }
            }
            _ => break,
        }
    }
    println!("Exiting Now!")
}
pub fn select_contact(contact: &mut Contact) -> Result<usize, Box<dyn std::error::Error>> {
    // Modify
    if contact.contact_information.len() >= 1 {
        list_contact_information(contact);

        // Finds the contact information
        let choice = uquery::num::<usize>(
            "Enter contact information number"
        );

        // Checks its in range
        if choice < contact.contact_information.len() {
            Ok(choice)
        } else {
            // Otherwise helps the user
            uquery::error(
                "Out of range",
                Some(
                    "Maybe you meant to select \
					something in range?",
                ),
            );
            Err(Box::from("Invalid Range"))
        }
    } else {
        // Help
        uquery::error(
            "No contact informations",
            Some("Maybe you meant to add one?"),
        );
        Err(Box::from("No Contacts"))
    }
}
pub fn add_contact_information(contact: &mut Contact) {
    loop {
        // Creates the contact information
        let mut ci = ContactInformation::new(
            String::new(), // No value is needed yet
            {
                // Prints the available choices
                println!(include_str!(
                    "../../data/modify/contact_information/platform_selector.txt",
                ));
                let choice = uquery::num::<u8>("Enter Choice");
                if choice < 11 && choice > 0 {
                    ContactPlatform::from_int(choice - 1)
                } else {
                    uquery::error(
                        "Invalid Choice, Exiting Now",
                        Some("Next time pick a valid choice."),
                    );
                    continue;
                }
            },
        );
        modify_contact_information(&mut ci);
        contact.contact_information.push(ci);
        break;
    }
}
pub fn modify_contact_information(ci: &mut ContactInformation) {
    // Value
    ci.value = uquery::string_with_default_reference("Enter the main value", Some(&ci.value));

    // Type
    ci.typ = Some({
        println!(include_str!("../../data/modify/contact/type.txt"));
        match uquery::num_with_default::<u8>(
            "Enter Choice",
            match &ci.typ {
                None => Some(2),
                Some(typ) => match typ {
                    Type::Work => Some(2),
                    _ => Some(1),
                },
            },
        ) {
            1 => Type::Home,
            2 => Type::Work,
            _ => {
                // Impossible
                println!("Invalid Choice");
                return ();
            }
        }
    });

    // Preference
    ci.pref = uquery::num_with_default::<u8>(
        "Enter the preference value",
        Some(ci.pref));
}

fn list_contact_information(contact: &mut Contact) {
    // Creates a pager to pipe the output to.
    let mut pager = minus::Pager::new();

    let mut n: u64 = 0;

    /*
     * Loops through all the contact information
     */
    for ci in &contact.contact_information {
        match {
            writeln!(
                pager.lines,
                "{}",
                format!(
                    " {:>6}| {:>8} {:>8} {:>60}",
                    n,
                    match &ci.typ {
                        None => "",
                        Some(typ) => {
                            match typ {
                                Type::Home => "Personal",
                                Type::Work => "Business",
                            }
                        }
                    },
                    ci.platform.to_string(),
                    ci.value
                )
            )
        } {
            Ok(()) => {}
            Err(e) => {
                eprintln!("Error Opening Pager \n {}", e);
            }
        }
        n += 1;
    }

    match minus::page_all(pager) {
        Ok(()) => {}
        _ => {
            eprintln!("Error Opening Pager.")
        }
    }
}
