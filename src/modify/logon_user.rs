use lrdb::logon_user::LogonUser;
use colored::Colorize;
use std::fmt::Write;
use crate::modify::contact::select_contact;

pub fn modify_logon_users (
    file: String
) -> Result<(), Box<dyn std::error::Error>> {
    let mut db = lrdb::database::Database::new_from_existing(file.as_str());

    // Prints the choice for the user and gets their query.
    println!(include_str!("../../data/modify/logon_user/logon_users.txt"));
    let choice = uquery::num::<u8>("Enter Choice");

    match choice {
        1 => {
	        add_logon_user(&mut db)
        }
        2 => {
            // Gets all the logon users
            let mut logon_users = db.restore_all_logon_users();

            // Gets a logon user to modify
			let logon_user = select_logon_user(&mut logon_users)?;

			// Updates it to the database
			update_logon_user(
    			&mut db,
    			&mut logon_users[logon_user]
			)
        },
        3 => {
            // Gets all the logon users
            let mut logon_users = db.restore_all_logon_users();

            // Gets a logon user to delete
			let logon_user = select_logon_user(&mut logon_users)?;

			// Deletes it from the database
			db.delete_logon_user(&logon_users[logon_user]);

			Ok(())
        },
        _ => {
            uquery::notice("Exiting Now", 3);
            return Ok(());
        }
    }
}

// Adds a logon user to a database
pub fn add_logon_user(
	db : &mut lrdb::database::Database,
) -> Result<(), Box<dyn std::error::Error>> {
	// Adds
	db.update_logon(
		// Creates
		&LogonUser::new (
			// Username
			uquery::string("Enter Username"),
			// Password
			rpassword::prompt_password_stdout(
				&format!(
					"{} Enter Password (Leave blank for no password): ",
					"::".bright_blue()
				)
			)?,
			// Permissions
			select_permission(None)
		)
	);
	
	Ok(())
}

// Updates a logon user to a database
pub fn update_logon_user(
	db : &mut lrdb::database::Database,
	logon_user: &mut LogonUser
) -> Result<(), Box<dyn std::error::Error>> {
    // Username
	logon_user.username = uquery::string_with_default_reference(
		"Enter Username",
		Some(&logon_user.username)
	);
	
	// Gets password
	let pass = rpassword::prompt_password_stdout(
		&format!(
			"{} Enter Password ( \
    			Enter a space for no password \
    			, enter nothing to use a pre existing password. \
			): ",
			"::".bright_blue()
		)
	)?;
	
	// Checks Password
	match pass.as_str() {
		" " => logon_user.hashed_password = String::new(), // Null Password
		"" 	=> {},
		_ 	=> logon_user.hashed_password = LogonUser::hash_pass(&pass)
	}

	logon_user.permissions = select_permission(
    	Some(
        	logon_user.permissions
    	)
	);

	// Asks if you would like to link it to a contact
	if uquery::boolean_with_default(
    	"Would you like to link to a contact?",
    	Some(false) // By default they don't want to do this
	) {
    	let contacts = db.restore_all_contacts()?;
		let contact = &contacts[select_contact(&contacts)];

		// Deletes the pre existing logon user
		db.delete_logon_user(logon_user);

		// Sets the new UUID
		logon_user.uuid = contact.uid.clone(); // Uh oh! A clone
	}
	
	// Adds
	db.update_logon(
    	logon_user
	);
	Ok(())
}

// Selects one of the logon users
pub fn select_logon_user(
	logons: &Vec<LogonUser>
) -> Result<usize, Box<dyn std::error::Error>> {
	loop {
		if logons.len() > 0 {
			list_logon_users(logons);

			// Select the contact
			let i = uquery::num::<usize>("Enter Choice");

			// Checks its in range
			if i < logons.len() {
				return Ok::<usize, Box<dyn std::error::Error>>(i);
			} else {
				uquery::error(
					"Out of range!",
					Some ("Try and enter something in range")
				)
			}
		} else {
			uquery::error(
				"There are no logon users",
				Some ("Maybe you meant to add one?")
			);
			return Err(Box::from("NoLogonUsers"));
		}
	}
}
// List all the logon users
pub fn list_logon_users(
	logons: &Vec<LogonUser>
) {
   // Creates a pager to pipe the output to.
    let mut pager = minus::Pager::new();

    let mut n: u64 = 0;
    // Loops through the contacts
    for logon in logons {
        match writeln!(
	        pager.lines,
	        " {:>6} | {:<10} {}",
	        n,
	        logon.permissions_to_string(),
	        logon.username
        ) {
            Ok(()) => {}
            _ => {
                eprintln!("Error Writing Line.")
            }
        }
        n += 1;
    }

    match minus::page_all(pager) {
        Ok(()) => {}
        _ => {
            eprintln!("Error Opening Pager.")
        }
    }
}
// Selects a permission level
pub fn select_permission(def : Option<u8>) -> u8 {
	loop {
		// Prints options
		println!(
			include_str!(
				"../../data/modify/logon_user/permissions.txt"
			)
		);
		// Queries for input
		let i = uquery::num_with_default::<u8>("Enter Choice", def);
		// Checks its valid
		if i >= 1 && i <= 4 {
			return i;
		}
	}
}
