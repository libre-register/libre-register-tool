mod contact;
mod contact_information;
mod logon_user;

//mod contact_information;
pub fn modify(
    mut args: std::env::Args
) -> Result<(), Box<dyn std::error::Error>> {
    // Skips past the first 2 arguments
    args.next();
    args.next();

    // Gets the file
    let file = args.next();

    match file {
        Some(file) => {
            // Notices
            uquery::notice("Database Modification.", 1);

            // Prints the choice for the user and gets their query
            println!(include_str!("../../data/modify/mod.txt"));
            let choice = uquery::num::<u8>("Enter Choice");

            // Matches the choice.
            match choice {
                // Contact
                1 => {
                    contact::modify_contacts(file);
                }
                // Logon User
                2 => {
					return logon_user::modify_logon_users(file);
                }
                // Modify Name
                3 => {
                    let db = lrdb::database::Database::new_from_existing(&file);
					db.change_name(
						&uquery::string_with_default_reference(
							"Please enter the school name",
							Some(&db.get_name()?),
						)
					)
                }
                _ => {
					println!("Invalid Choice, Exiting Now!");
                }
            }
        }
        None => eprintln!("Error! No file specified."),
    }
    Ok(())
}
